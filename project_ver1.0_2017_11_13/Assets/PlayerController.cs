﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerController : MonoBehaviour {
    //変数定義
    public float jump_power = 1000f;  //ジャンプの高さ
    public float scroll = 5f;   //なぞ

    public int lieDown = 0; //伏せ状態か否か。0で立ち、1で伏せ
    public int contactBar = 0; //引っかかり状態か否か
    public int modeStatus = 0;  //主人公のモード状態。0で鉄、1でゴム。罠などとの各種判定に使用
    //public int WindContact = 0; //プレイヤーと風が衝突しているか
    //public int H_WindContact = 0;//プレイヤーと熱風が衝突しているか
    //public int E_WindContact = 0;//プレイヤーと電気風が衝突しているか

    public Material[] _material;//キャラクターのグラフィック
    float direction = 0f;   //移動量
    Rigidbody2D rb2d;   
    bool jump = false;  //ジャンプ中か否か


    private GameObject manager_;    /* GameManager読み込みよう変数 */

    // Use this for initialization
    void Start()
    {
        //コンポーネント読み込み
        rb2d = GetComponent<Rigidbody2D>();

        manager_ = GameObject.Find("GameManager");  /* GameManager読み込み */
    }

    void ModeChange()
    {
        if(modeStatus==0)   //鉄状態からゴム状態へ
        {
            modeStatus++;   //ステータスの変更。ゴムへ
            //モード変更のアニメーションなど？
            this.GetComponent<Renderer>().material = _material[0];//グラフィックの変更
        }
        else                //ゴム状態から鉄状態へ
        {
            modeStatus--;   //ステータスの変更。鉄へ
            //モード変更のアニメーションなど？
            this.GetComponent<Renderer>().material = _material[1];//グラフィックの変更。鉄へ
        }        
    }

    // Update is called once per frame
    void Update()
    {

        // ジャンプ処理
        if(Input.GetButtonDown("Jump"))
        {
            this.GetComponent<Rigidbody2D>().AddForce(new Vector2(0, jump_power));
        }

        //キーボード操作
        if (Input.GetKey(KeyCode.RightArrow))   //右移動
        {
            direction = 1f;
        }
        else if (Input.GetKey(KeyCode.LeftArrow))   //左移動
        {
            direction = -1f;
        }
        else if (Input.GetKeyDown(KeyCode.DownArrow))    //伏せ
        {
            //伏せる処理。専用グラフィック。移動付加
            this.GetComponent<MeshRenderer>().material.color = Color.red;
            lieDown++;
        }
        else if (Input.GetKeyUp(KeyCode.DownArrow))
        {
            //伏せ解除時の処理
            this.GetComponent<MeshRenderer>().material.color = Color.white;
            lieDown--;
        }
        else if (Input.GetKeyDown(KeyCode.UpArrow))
        {
            ModeChange();
        }
        else    //ニュートラル状態
        {
            direction = 0f;
        }
        //キャラのy軸のdirection方向にscrollの力をかける
        rb2d.velocity = new Vector2(scroll * direction, rb2d.velocity.y);

        ////ジャンプ判定
        //if (Input.GetKeyDown("space") && !jump)
        //{
        //    rb2d.AddForce(Vector2.up * jump_power);
        //    jump = true;
        //}
    }
     void  OnCollisionEnter2D(Collision2D other)  
    {
        if (other.gameObject.CompareTag("Ground"))  //地形と接触しているかの判定
        {
            jump = false;
        }
    }
        
     void OnTriggerEnter2D(Collider2D info)
    {
        if (info.tag == "Bar")  //バーと接触してるかどうか
        {
            contactBar++;
        }
        if (info.tag == "HotNeedle") //熱いハリとぶつかった時
        {
            if (lieDown == 1)   //伏せているか
            {
                if (modeStatus == 0)    //鉄状態だったら
                {
                    //アニメーション開始
                }
                else　 //違ったら（ゴムだった場合）
                {
                    manager_.GetComponent<GameManager>().Miss();     //ゲームオーバーへ
                }
            }
            else
            {
                manager_.GetComponent<GameManager>().Miss();     //ゲームオーバーへ
            }
        }
        if (info.tag == "ElectricNeedle") //電気ハリとぶつかった時
        {
            if (lieDown == 1)   //伏せているか
            {
                if (modeStatus == 1)    //ゴム状態だったら
                {
                    //アニメーション開始
                }
                else　 //違ったら（鉄だった場合）
                {
                    manager_.GetComponent<GameManager>().Miss();     //ゲームオーバーへ
                }
            }
            else
            {
                manager_.GetComponent<GameManager>().Miss();     //ゲームオーバーへ
            }
        }
        if (info.tag == "Wind")
        {
            if (contactBar == 1 && lieDown == 1)
            {
                //指定のアニメーション
            }
             else//それ以外ゲームオーバー
             {
                 manager_.GetComponent<GameManager>().Miss();
             }
        }
        if (info.tag == "HotWind")
        {
            if (modeStatus == 0 && contactBar == 1 && lieDown == 1)
            {
                //指定のアニメーション
            }
            else//それ以外ゲームオーバー
            {
                manager_.GetComponent<GameManager>().Miss();
            }
        }
        if (info.tag == "ElectricWind")
        {             if (modeStatus == 1 && contactBar == 1 && lieDown == 1)
            {
                //指定のアニメーション
            }
            else//それ以外ゲームオーバー
            {
                manager_.GetComponent<GameManager>().Miss();
            }
        }
    }
    void OnTriggerExit2D(Collider2D info)   //離れたら
     {
         if (info.tag == "Bar")
         {
             contactBar--;
         }
     }
}