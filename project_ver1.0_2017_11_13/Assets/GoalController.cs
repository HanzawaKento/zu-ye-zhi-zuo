﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GoalController : MonoBehaviour {

    public GameObject player;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    void OnTriggerStay2D(Collider2D info)   //プレイヤーと衝突時ゲームオーバー
    {
        if (info.gameObject.name == "Player")
        {
            player = GameObject.Find("Player");
            {
                SceneManager.LoadScene("GameClear");
            }
        }
    }
}
