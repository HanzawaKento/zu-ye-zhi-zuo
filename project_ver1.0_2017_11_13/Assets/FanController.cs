﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FanController : MonoBehaviour {
    public GameObject wind;
    Rigidbody2D rb2D;
    public int wind_Pos_x = 0;//風の発生位置


	// Use this for initialization
	void Start () {
        Invoke("Wind",3.0f);    //３秒おきに実行
	}

    void Wind() //指定の位置に風を発生させて、３秒後に破壊し、以後ループ
    {
            GameObject obj = Instantiate(wind, new Vector3(transform.position.x + wind_Pos_x, transform.position.y, 0), this.transform.rotation);
            obj.transform.eulerAngles = new Vector3(0, 90, 0);
            Destroy(obj, 3);
            Invoke("roop", 3.0f);
    }
    void roop() //ループ用の処理
    {
        Invoke("Wind",3.0f);
    }

	// Update is called once per frame
	void Update () {
		
	}
}
