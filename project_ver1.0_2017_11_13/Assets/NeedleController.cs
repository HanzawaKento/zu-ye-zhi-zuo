﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class NeedleController: MonoBehaviour {

    public GameObject player;
    public GameObject Shadow;
    public float fallSpeed = 5f;    //針の落下速度
    Rigidbody2D rb2d;
    int count = 0;  //陰を複数発生させないためのカウント
    public int fallDistance = 0;    //ハリが落下開始する距離。個別設定用
    public int shadowPos;   //影の発生位置の個別設定用

	// Use this for initialization
    void Start()
    {



    }

	// Update is called once per frame
	void Update () {
        //プレイヤーと一定の距離になったら
        Vector2 PlayerPos = player.transform.position;  //プレイヤーの位置情報取得
        Vector2 NeedlePos = this.transform.position;  //針の位置情報取得
        float dis = Vector2.Distance(PlayerPos,NeedlePos);  //プレイヤーと針の距離取得

        if (dis <= fallDistance && count == 0)
        {

            //落下地点に影を発生させる
            RaycastHit2D hit;
            hit = Physics2D.Raycast(NeedlePos, new Vector2(0, -1));
            print(hit.point);
            Instantiate(Shadow, hit.point, this.transform.rotation);
            count++;
            //その後針が落下する
            GetComponent<Rigidbody2D>().velocity = new Vector2(0, -1*fallSpeed);
            //プレイヤーと接触した場合、プレイヤーが立ち状態ならゲームオーバー
            //適切な伏せ状態なら回避可能
        }
        
    }
    void OnTriggerEnter2D(Collider2D info)   //プレイヤーと衝突時ゲームオーバー
    {
        if (info.name == "Player")
        {
            SceneManager.LoadScene("GameOver");
        }
    }
}
