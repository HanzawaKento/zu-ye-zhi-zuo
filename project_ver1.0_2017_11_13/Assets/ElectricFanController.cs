﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ElectricFanController : MonoBehaviour {
    public GameObject electricWind;
    Rigidbody2D rb2D;
    public int wind_Pos_x = 0;

    // Use this for initialization
    void Start()
    {
        Invoke("Wind", 3.0f);
    }

    void Wind()
    {
        GameObject obj = Instantiate(electricWind, new Vector3(transform.position.x + wind_Pos_x, transform.position.y, 0), this.transform.rotation);
        obj.transform.eulerAngles = new Vector3(0, 90, 0);
        Destroy(obj, 3);
        Invoke("roop", 3.0f);
    }
    void roop()
    {
        Invoke("Wind", 3.0f);
    }

    // Update is called once per frame
    void Update()
    {

    }
}