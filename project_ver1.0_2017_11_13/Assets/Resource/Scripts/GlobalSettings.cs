﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/* ゲーム全体で使用するような値は、ここで宣言します
 */

public class GlobalSettings
{
    private static string scene_name_ = "Play_Level1";     /* 現在のシーンの名前 */

    public static string sceneName { get { return scene_name_; } set { scene_name_ = value; } }
}
