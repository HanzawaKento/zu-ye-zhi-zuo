﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StockManager : MonoBehaviour {

    private GameObject manager_;

	// Use this for initialization
	void Start ()
    {
        manager_ = GameObject.Find("GameManager");
        this.GetComponent<Text>().text = manager_.GetComponent<GameManager>().playerLife + "";
	}
}
