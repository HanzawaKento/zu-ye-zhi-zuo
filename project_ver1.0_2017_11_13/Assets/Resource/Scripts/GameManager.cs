﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using System;

public class GameManager : MonoBehaviour
{
    [SerializeField, Range(0, 3)]
    private int player_life_max_;   /* 残機の最大 */

    private int player_life_now_;   /* 現在の残機 */

    void Awake()
    {
        player_life_now_ = player_life_max_;
        DontDestroyOnLoad(this);    /* 自身をシーン遷移で破壊されなくする */
    }

    /* ミスをした場合この関数を呼び出します */
    public void Miss()
    {
        player_life_now_--;
        if (player_life_now_ > -1)
        {
            SceneManager.LoadScene(GlobalSettings.sceneName);
        }
        else
        {
            player_life_now_ = player_life_max_;
            SceneManager.LoadScene("GameOver");
        }
    }

    public int playerLife { get { return player_life_now_; } set { player_life_now_ = value; } }
}